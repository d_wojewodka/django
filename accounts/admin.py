from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.auth.forms import (
    AdminPasswordChangeForm,
    UserChangeForm,
    UserCreationForm,
)
from .models import Account

    
# Register your models here.

# class AccountChangeForm(UserChangeForm):
#     class Meta:
#         model = Account
#         fields = "__all__"
        
# class AccountCreationForm(UserCreationForm):
#     class Meta(UserCreationForm.Meta):
#         model = Account
#         fields = "__all__"
      
        
class AccountAdmin(UserAdmin):
    fieldsets = (
        (
            None, 
            {
                "fields": (
                    "username", 
                    "email", 
                    "password",
                )
            }
        ),
        # ("Personal info", {"fields": ("first_name", "last_name")}),
        (
            "Permissions",
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                ),
            },
        ),
        # ("Important dates", {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": (
                    "wide",
                ),
                "fields": (
                    "username", 
                    "email", 
                    "password1", 
                    "password2",
                ),
            },
        ),
    )

    list_display = (
        "username", 
        "email", 
        "last_login", 
        "is_staff",
    )
    
    list_filter = (
        "is_staff", 
        "is_superuser", 
        "is_active",
    )
    
    search_fields = (
        "username", 
        "email",
    )
    
    ordering = (
        "username",
    )
    
    filter_horizontal = ()
    # filter_horizontal = (
    #     "many-to-many",
    # )


admin.site.register(Account, AccountAdmin)