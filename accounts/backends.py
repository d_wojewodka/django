from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User
from .models import Account

class EmailAuthBackend(BaseBackend):
    def authenticate(self, request, username=None, password=None):
        """
        Overrides the authenticate method to allow users to log in using their email address.
        """
        if username is None or password is None:
            return
        
        print("check1")
        
        try:
            user = Account.objects.get(email = username)
            
            print("check1")
            
            if user.check_password(password):
                return user
            
            
        except Account.DoesNotExist:
            """
            Run the default password hasher once to reduce the timing
            difference between an existing and a nonexistent user (#20760).
            """
            Account().set_password(password)
        
    def get_user(self, user_id):
        """
        Overrides the get_user method to allow users to log in using their email address.
        """
        try:
            return Account.objects.get(pk=user_id)
        except Account.DoesNotExist:
            return None 
        

class UsernameAuthBackend(BaseBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        """
        Overrides the authenticate method to allow users to log in using their email address.
        """
        
        if username is None or password is None:
            return
        try:
            user = Account.objects.get(username=username)
    
            if user.check_password(password):
                return user
        except Account.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            Account().set_password(password)
    

    def get_user(self, user_id):
        """
        Overrides the get_user method to allow users to log in using their email address.
        """
        try:
            return Account.objects.get(pk=user_id)
        except Account.DoesNotExist:
            return None