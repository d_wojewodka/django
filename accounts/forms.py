from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError

# Import custom user model
from .models import Account
UserModel = Account()


class AccountCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""

    username = forms.CharField(label='Username', min_length=4, max_length=150)
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)


