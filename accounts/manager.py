from django.db import models
from django.contrib.auth.models import BaseUserManager


class AccountManager(BaseUserManager):
    use_in_migrations = True
    
    def create_user(self, username, email, password=None):
        """
        Creates and saves a User with the given email
        """
        if not email:
            raise ValueError('Email must be provided')
        
        user=self.model(
            username=username,
            email=self.normalize_email(email),
        )
        
        user.set_password(password)
        user.save(using=self._db)
        
        return user

    def create_superuser(self, username, email, password=None):
        """
        Creates and saves a User with the given email
        """
        if not email:
            raise ValueError('Email must be provided')
        
        user=self.model(
            username=username,
            email=self.normalize_email(email),
        )
        
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        
        return user
