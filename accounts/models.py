from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils import timezone
from .manager import AccountManager
# Create your models here.


class Account(AbstractBaseUser):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
    Username and password are required. Other fields are optional.
    """
    
    #username
    username = models.CharField(verbose_name='username',
                                max_length=150,
                                unique=True,
    )
    
    #email
    email=models.EmailField(verbose_name='email address', 
                            max_length=60, 
                            unique=True)
    
    # #first name
    # first_name = models.CharField(verbose_name='first name', 
    #                               max_length=150, 
    #                               blank=True)
    
    # #last name
    # last_name = models.CharField(verbose_name='last name', 
    #                              max_length=150, 
    #                              blank=True)
    
    #date joined
    date_joined = models.DateTimeField(verbose_name='date joined',
                                       default=timezone.now,
    )
    
    #last login
    last_login = models.DateTimeField(verbose_name='last login', 
                                      auto_now=True,
    )
    
    #is active
    is_active = models.BooleanField(verbose_name='active',
                                    default=True,
    )

    #is staff
    is_staff = models.BooleanField(verbose_name='staff',
                                   default=False,
    )
    
    #is superuser
    is_superuser = models.BooleanField(verbose_name='superuser', 
                                      default=False
    )
       
    objects = AccountManager()
     
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        # "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True