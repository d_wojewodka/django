import graphene
from graphene_django import DjangoObjectType
from .models import Account
from .backends import EmailAuthBackend, UsernameAuthBackend

class AccountType(DjangoObjectType):
    class Meta:
        model = Account
        # fields = ("id", "username", "email")
        fields = ("username",)
        
class Query(graphene.ObjectType):
    auth_by_login = graphene.Field(AccountType, login=graphene.String(required=True), password=graphene.String(required=True))
    
    def resolve_auth_by_login(self, info, login, password):
        try:
            if '@' in login:
                auth = EmailAuthBackend()
            else:
                auth = UsernameAuthBackend()
                
            return auth.authenticate(info, login, password)
        except Account.DoesNotExist:
            return None
        # authByLogin(login: "domniq", password: "domniq") {
        #     id
        #     username
        #     email
        # }
    
    
    """
    auth_by_username = graphene.Field(AccountType, username=graphene.String(required=True), password=graphene.String(required=True))
    auth_by_email = graphene.Field(AccountType, email=graphene.String(required=True), password=graphene.String(required=True))
    
    
    def resolve_auth_by_username(self, info, username, password):
        try:
            auth = UsernameAuthBackend()
            return auth.authenticate(info, username, password)
        except Account.DoesNotExist:
            return None
        # authByEmail(email: "domniq@email.com", password: "domniq") {
        #     id
        #     username
        #     email
        # }
        
        
    def resolve_auth_by_email(self, info, email, password):
        try:
            auth = EmailAuthBackend()
            return auth.authenticate(info, email, password)
        except Account.DoesNotExist:
            return None
        # authByUsername(username: "domniq", password: "domniq") {
        #     id
        #     username
        #     email
        # }
    """

        
schema = graphene.Schema(query=Query)